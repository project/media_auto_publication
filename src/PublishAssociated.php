<?php

declare(strict_types=1);

namespace Drupal\media_auto_publication;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Publish associated medias.
 */
final class PublishAssociated {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs an "PublishAssociated" object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity type manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Publish associated medias.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function publish(EntityInterface $entity): void {
    if (self::isBeingPublished($entity)) {
      $this->publishReferencedMedias($entity);
    }
  }

  /**
   * Check entity has publish state and is being published.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return bool
   *   The entity is being published or not.
   */
  private static function isBeingPublished(EntityInterface $entity): bool {
    if (
      $entity instanceof EntityPublishedInterface &&
      $entity instanceof FieldableEntityInterface &&
      $entity->isPublished() &&
      (
        $entity->original === NULL ||
        (
          $entity->original instanceof EntityPublishedInterface &&
          !$entity->original->isPublished()
        )
      )
    ) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Publish referenced medias.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function publishReferencedMedias(EntityInterface $entity): void {
    // Get all fields attached of entity.
    $fields = $this->entityFieldManager->getFieldDefinitions(
      $entity->getEntityTypeId(),
      $entity->bundle()
    );

    // Loop to find all fields of type "entity_reference"
    // with "media" target type.
    foreach ($fields as &$field) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field */

      if (
        $field->getType() !== 'entity_reference' ||
        $field->getSetting('target_type') !== 'media'
      ) {
        continue;
      }

      // Get referenced entities.
      $referenced_entities = $entity->get($field->getName())->referencedEntities();
      foreach ($referenced_entities as $referenced_entity) {
        /** @var \Drupal\media\MediaInterface $referenced_entity */

        if ($referenced_entity->isPublished()) {
          continue;
        }

        // Publish entity.
        $referenced_entity
          ->setPublished(TRUE)
          ->save();
      }
    }
  }

}
