## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


## INTRODUCTION

This module allows to automatically publish medias when associated entity is being published.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_auto_publication

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/media_auto_publication


## REQUIREMENTS

Based on "Media" and "Field" core modules.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## MAINTAINERS

Current maintainers:
 * Sébastien Brindle (S3b0uN3t) - https://www.drupal.org/u/s3b0un3t
